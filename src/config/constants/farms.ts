import contracts from './contracts'
import { FarmConfig, QuoteToken } from './types'

const farms: FarmConfig[] = [
  {
    pid: 4,
    risk: 2,
    lpSymbol: 'PSY-BNB LP',
    lpAddresses: {
      97: '',
      56: '0xf48d05e9714d93d730b587f258898b864a13d83d',
    },
    tokenSymbol: 'PSY',
    tokenAddresses: {
      97: '',
      56: '0x8dbc995946ad745dd77186d1ac10019b8ea6694a',
    },
    quoteTokenSymbol: QuoteToken.BNB,
    quoteTokenAdresses: contracts.wbnb,
  },
  {
    pid: 5,
    risk: 1,
    lpSymbol: 'PSY-BUSD LP',
    lpAddresses: {
      97: '',
      56: '0xf053b44a2db0ecc45cc67ab0ecdaa73842a81139',
    },
    tokenSymbol: 'PSY',
    tokenAddresses: {
      97: '',
      56: '0x8dbc995946ad745dd77186d1ac10019b8ea6694a',
    },
    quoteTokenSymbol: QuoteToken.BUSD,
    quoteTokenAdresses: contracts.busd,
  },
  {
    pid: 6,
    risk: 2,
    lpSymbol: 'PSY-CAKE LP',
    lpAddresses: {
      97: '',
      56: '0x07145d0ae092a61ef5048db9899328c43b2ee35a',
    },
    tokenSymbol: 'PSY',
    tokenAddresses: {
      97: '',
      56: '0x8dbc995946ad745dd77186d1ac10019b8ea6694a',
    },
    quoteTokenSymbol: QuoteToken.CAKE,
    quoteTokenAdresses: contracts.cake,
  },
  {
    pid: 7,
    risk: 3,
    lpSymbol: 'BNB-BUSD LP',
    lpAddresses: {
      97: '',
      56: '0x58f876857a02d6762e0101bb5c46a8c1ed44dc16',
    },
    tokenSymbol: 'BNB',
    tokenAddresses: {
      97: '',
      56: '0xbb4cdb9cbd36b01bd1cbaebf2de08d9173bc095c',
    },
    quoteTokenSymbol: QuoteToken.BUSD,
    quoteTokenAdresses: contracts.busd,
  },
]

export default farms
