import React from 'react'
import styled from 'styled-components'
import { Card, CardBody, Heading, Skeleton, Text } from '@pancakeswap-libs/uikit'
import useI18n from 'hooks/useI18n'
// import { useGetStats } from 'hooks/api'
import { useTotalValue, usePriceCakeBusd } from '../../../state/hooks'
import CardValue from './CardValue'
import {useWindowSize} from '../../psymarket'

const StyledTotalValueLockedCard = styled(Card)`
  align-items: center;
  display: flex;
  flex: 1;
`

const TotalValueLockedCard = () => {
  const TranslateString = useI18n()
  // const data = useGetStats()
  const totalValue = useTotalValue();
  // const tvl = totalValue.toFixed(2);
  const cakePriceUsd = usePriceCakeBusd()
  const size = useWindowSize()

  return (
    <StyledTotalValueLockedCard>
      <CardBody>
        <Heading size="ml" mb="24px" color="tertiary" mt={size.width >= 992 ? "-110px" : "0px"}>
          {TranslateString(999, 'Total Value Locked (TVL)')}
        </Heading>
        {/* <Heading size="xl">{`$${tvl}`}</Heading> */}
        {/* <Heading size="xl"> */}
          <CardValue value={totalValue.toNumber()} prefix="$" decimals={2} color="fourth"/>
        {/* </Heading> */}
        <Text color="tertiary">{TranslateString(999, 'Across all Farms and Pools')}</Text>
        <Heading size="lg" mb="24px" paddingTop="50px" color="tertiary">
          {TranslateString(999, 'Psychic Price')}
        </Heading>
        {/* <Heading size="xl">{`$${tvl}`}</Heading> */}
        {/* <Heading size="xl"> */}
        <CardValue value={cakePriceUsd.toNumber()} prefix="$" decimals={2} color="fourth" />
        {/* </Heading> */}
        {/* <Text color="tertiary">{TranslateString(999, 'BUY ON PANCAKESWAP')}</Text>   */}
      </CardBody>
    </StyledTotalValueLockedCard>
  )
}

export default TotalValueLockedCard
