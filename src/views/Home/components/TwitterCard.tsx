import React from 'react'
import { Card, CardBody, Heading } from '@pancakeswap-libs/uikit'
// import BigNumber from 'bignumber.js/bignumber'
import styled from 'styled-components'
import { Timeline } from 'react-twitter-widgets'
// import { getBalanceNumber } from 'utils/formatBalance'
// import { useTotalSupply, useBurnedBalance } from 'hooks/useTokenBalance'
import useI18n from 'hooks/useI18n'
import useTheme from 'hooks/useTheme';
// import { getCakeAddress } from 'utils/addressHelpers'
// import CardValue from './CardValue'
// import { useFarms } from '../../../state/hooks'

const StyledTwitterCard = styled(Card)`
  margin-left: auto;
  margin-right: auto;
`

const Row = styled.div`
  align-items: center;
  display: flex;
  font-size: 14px;
  justify-content: space-between;
  margin-bottom: 8px;
`

const TwitterCard = () => {
  const TranslateString = useI18n()
  const { isDark } = useTheme()

  return (
    <StyledTwitterCard>
      <CardBody>
        <Heading size="ml" mb="24px" color="tertiary">
          {TranslateString(999, 'PSY News')}
        </Heading>
        <Timeline
          dataSource={{
            sourceType: 'profile',
            screenName: 'FinancePsychic'
          }}
          options={{
            theme: isDark? 'dark' : 'light',
            height: '300',
            chrome: "noheader, nofooter",
            width: "400",
          }}
        />
      </CardBody>
    </StyledTwitterCard>
  )
}

export default TwitterCard
