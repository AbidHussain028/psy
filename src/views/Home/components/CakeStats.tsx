import React from 'react'
import { Card, CardBody, Heading, Text } from '@pancakeswap-libs/uikit'
import BigNumber from 'bignumber.js/bignumber'
import styled from 'styled-components'
import { getBalanceNumber } from 'utils/formatBalance'
import useTotalLocked from 'hooks/useTotalUserLocked'
import useCanUnlockAmount from 'hooks/useCanUnlockAmount'
import { useTotalSupply, useBurnedBalance } from 'hooks/useTokenBalance'
import useI18n from 'hooks/useI18n'
import { getCakeAddress } from 'utils/addressHelpers'
import CardValue from './CardValue'
import { useFarms, usePriceCakeBusd } from '../../../state/hooks'

const StyledCakeStats = styled(Card)`
  margin-left: auto;
  margin-right: auto;
`

const Row = styled.div`
  align-items: center;
  display: flex;
  font-size: 14px;
  justify-content: space-between;
  margin-bottom: 8px;
`

const CakeStats = () => {
  const TranslateString = useI18n()
  const totalSupply = useTotalSupply()
  const userLocked = useTotalLocked()
  const canUnlock = useCanUnlockAmount()
  const burnedBalance = useBurnedBalance(getCakeAddress())
  const farms = useFarms();
  const psyPrice = usePriceCakeBusd();
  const circSupply = totalSupply ? totalSupply.minus(burnedBalance) : new BigNumber(0);
  const cakeSupply = getBalanceNumber(circSupply);
  const marketCap = psyPrice.times(circSupply);
  const currentFarmed = circSupply ? circSupply.times(0.25) : new BigNumber(0);
  const totalLocked = circSupply ? circSupply.times(0.75) : new BigNumber(0);

  let REWARD_PER_BLOCK = 0;
  if(farms && farms[0] && farms[0].REWARD_PER_BLOCK){
    REWARD_PER_BLOCK = new BigNumber(farms[0].REWARD_PER_BLOCK).div(new BigNumber(10).pow(18)).toNumber();
  }

  return (
    <StyledCakeStats>
      <CardBody>
        <Heading size="ml" mb="24px" color="tertiary">
          {TranslateString(999, 'PSY Stats')}
        </Heading>
        <Row>
          <Text fontSize="17px">{TranslateString(10005, 'Market Cap')}</Text>
          <CardValue fontSize="17px" value={getBalanceNumber(marketCap)} decimals={0} prefix="$" color="tertiary" />
        </Row>
        <Row>
          <Text fontSize="17px">{TranslateString(999, 'Circulating Supply')}</Text>
          {cakeSupply && <CardValue fontSize="17px" value={cakeSupply} decimals={0} color="tertiary" />}
        </Row>
        <Row>
          <Text fontSize="17px">{TranslateString(999, 'Total Burned')}</Text>
          <CardValue fontSize="17px" value={getBalanceNumber(burnedBalance)} decimals={0} color="tertiary" />
        </Row>
        <Row>
          <Text fontSize="17px">{TranslateString(999, 'New PSY/block')}</Text>
          <Text bold fontSize="17px" color="tertiary">{REWARD_PER_BLOCK}</Text>
        </Row>
        <Row>
          <Text fontSize="17px">{TranslateString(999, 'Total farmed amount')}</Text>
          <CardValue fontSize="17px" value={getBalanceNumber(currentFarmed)} decimals={0} color="tertiary" />
        </Row>
        <Row>
          <Text fontSize="17px">{TranslateString(999, 'Total contract locked amount')}</Text>
          <CardValue fontSize="17px" value={getBalanceNumber(totalLocked)} decimals={0} color="tertiary" />
        </Row>
        <Row>
          <Text fontSize="17px">{TranslateString(999, 'Your PSY locked amount')}</Text>
          <CardValue fontSize="17px" value={getBalanceNumber(userLocked)} decimals={0} color="tertiary" />
        </Row>
        <Row>
          <Text fontSize="17px">{TranslateString(999, 'PSY Unlocked')}</Text>
          <CardValue fontSize="17px" value={parseFloat(getBalanceNumber(canUnlock).toFixed(4))} decimals={0} color="tertiary" />
        </Row>
      </CardBody>
    </StyledCakeStats>
  )
}

export default CakeStats
