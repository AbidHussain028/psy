import React from 'react'
import styled from 'styled-components'
import ReactPlayer from 'react-player'
import { Image } from '@pancakeswap-libs/uikit'
import Page from 'components/layout/Page'
// import useI18n from 'hooks/useI18n'
import useTheme from 'hooks/useTheme';
import {useWindowSize} from './psymarket'

const StyledNotFound = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  // height: calc(100vh - 64px);
  justify-content: center;
`

const CardImage = styled.img`
  margin-bottom: 16px;
`

const NotFound = () => {
  // const TranslateString = useI18n()
  const { isDark, theme } = useTheme();
  const size = useWindowSize()

  return (
    <Page>
      <StyledNotFound>
        <ReactPlayer 
          url="https://youtu.be/8-kG_DYzzdM"
          width={size.width >= 992 ? "80%" : "100%"}
          controls
        />
        <img src="/images/final_referre.gif" alt=""/>
        {isDark ? <Image src="/images/predict_night.jpg" alt="market" width={1280} height={1183} responsive />
        :<Image src="/images/predict_day.jpg" alt="market" width={1280} height={1183} responsive />}
        {isDark ? <Image src="/images/coming_soon_night.png" alt="comping" width={1280} height={730} responsive />
        :<Image src="/images/coming_soon_day.png" alt="coming" width={1280} height={730} responsive />}
      </StyledNotFound>
    </Page>
  )
}

export default NotFound
