import React, { useState, useEffect } from "react";
import Web3 from "web3";
import "bootstrap/dist/css/bootstrap.min.css";
import "./responsivenav.css";
import bell from "../../../../assets/images/favicon.ico";
// import logo from "../../../../assets/images/logo.jpg";
import menuIcon from "../../../../assets/images/menuIcon.png";
import { contractAddress, contractabi } from "../../../../utils/constant";

const Navigatmenu = () => {
  const [account, setAccount] = useState("Connect To Wallet");

  const [enteredAmount, setEnteredAmount] = useState(0);
  const [rewards, setRewards] = useState(0);
  const [message, setMessage] = useState({
    show: false,
    severity: "",
    message: "",
    title: "",
  });
  const [state, setState] = useState({
    mainAccount: null,
    totalZinTokens: null,
    rewards: null,
    stakeOf: null,
  });

  const loadWeb3 = async () => {
    let isConnected = false;
    try {
      if (window.ethereum) {
        window.web3 = new Web3(window.ethereum);
        await window.ethereum.enable();

        isConnected = true;
      } else if (window.web3) {
        window.web3 = new Web3(window.web3.currentProvider);
        isConnected = true;
      } else {
        isConnected = false;
        const obj = {
          show: true,
          severity: "error",
          message:
            "Metamask is not installed, please install it on your browser to connect.",
        };
        setMessage(obj);
      }
      if (isConnected === true) {
        const web3 = window.web3;
        const contract = new web3.eth.Contract(contractabi, contractAddress);
        const accounts = await getAccounts();
        setAccount(accounts[0]);

        setState({
          mainAccount: accounts[0],
        });

        const accountDetails = null;
        window.ethereum.on("accountsChanged", function (_accounts) {
          //   clearInterval(rev);
          setAccount(_accounts[0]);
          // getUpdateAccount(accounts);
          console.log("accounts", _accounts);
          localStorage.setItem("load", _accounts[0]);
        });
      }
    } catch (error) {
      console.log(error);
      const obj = {
        show: true,
        severity: "error",
        message: "Error while connecting metamask",
        title: "Connecting Metamask",
      };
      setMessage(obj);
    }
  };

  const getAccounts = async () => {
    const web3 = window.web3;
    try {
      const accounts = await web3.eth.getAccounts();
      console.log(accounts);
      return accounts;
    } catch (error) {
      console.log("Error while fetching acounts: ", error);
      return null;
    }
  };
  // eslint-disable-next-line
  const isLockedAccount = async () => {
    try {
      const accounts = await getAccounts();
      if (accounts.length > 0) {
        console.log("Metamask is unlocked");
      } else {
        console.log("Metamask is locked");
      }
    } catch (error) {
      alert("Error while checking locked account");
    }
  };
  // eslint-disable-next-line
  const getBalanceOfAccount = async () => {
    const mainAccount = await getAccounts();
    const web3 = window.web3;
    web3.eth.getBalance(mainAccount[0], function (err, res) {
      console.log("Resp: ", res);
    });
  };
  const [mainAccountDetails, setMainAccountDetails] = useState(null);

  // useEffect(() => {
  //   setInterval(() => {
  //     if (account) {
  //       loadWeb3();
  //     } else {
  //       loadWeb3();
  //     }
  //   }, 1000);
  //   // eslint-disable-next-line
  // }, []);
  const [showLinks, setShowLinks] = useState(false);
  return (
    <div className="Navbar" style={{ position: "relative", zIndex: "1" }}>
      <div className="rightSide">
        <img className="logo" src={bell} alt="Logo" />
      </div>
      <div className="leftSide">
        <div className="links" id={showLinks ? "hidden" : ""}>
          {/* <a href="#home">Home</a> */}
          {/* <a href="#news">What is TTTM</a>
                    <a href="#home">Smart Contract</a>
                    <a href="#news">Audit</a> */}
          {/* <a href="#home">Telgram</a> */}
          <a
            href={`https://bscscan.com/address/${account}`}
            target="_blank"
            rel="noreferrer"
          >
            <button type="button" id="connect" onClick={loadWeb3}>
              {account}
            </button>
          </a>
          {/* <a href="#home">
                        <img src={bell} />
                    </a> */}
        </div>
        <button type="button" onClick={() => setShowLinks(!showLinks)}>
          <img src={menuIcon} alt="img" />
        </button>
      </div>
    </div>
  );
};

export default Navigatmenu;
