import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import { toast } from "react-toastify";
import Web3 from "web3";
import { useWallet } from '@binance-chain/bsc-use-wallet'

import {
  contractAddress,
  tokeAddress,
  contractabi,
  tokenabi,
} from "../../../../utils/constant";

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

export default function CustomizedDialogs(props) {
  const { dialogueAccount, open } = props;
  const [value, setValue] = useState("");
  const [points, setPoints] = useState("");
  const [prizeAmount, setPrizeAmount] = useState("");
  const { account ,ethereum} = useWallet()

  useEffect(() => {
    setValue("");
    setPoints("");
  }, [open]);

  const handleClose = () => {
    props.handleCloseModel();
  };
  const transferPoints = async () => {
    try {
      if (dialogueAccount && dialogueAccount !== "") {
        const web3 = new Web3(ethereum)

        const contract = new web3.eth.Contract(contractabi, contractAddress);
        const tokencontract = new web3.eth.Contract(tokenabi, tokeAddress);

        const depositTokens = await contract.methods
          .transferPoints(value, web3.utils.toWei(points))
          .send({
            from: account,
          })
          .on("transactionHash", async (hash) => {
            toast.info("Your transaction is pending");
          })
          .on("receipt", async (receipt) => {
            toast.success("Your transaction is confirmed");
            console.log("confrimed", receipt);
          })
          .on("error", async (error) => {
            console.log("error", error);
            toast.error("User denied transaction");
          });
      } else {
        toast.error("Not Connected To Metamask network");
      }
    } catch (e) {
      console.log("dialog error", e);
    }
  };
  return (
    <div>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          Transfer Points
        </DialogTitle>
        <DialogContent dividers>
          <span>address</span>
          <TextField
            value={value}
            style={{ minWidth: "230px", display: "block", margin: "1px" }}
            id="outlined-basic"
            type="string"
            variant="outlined"
            onChange={(e) => setValue(e.target.value)}
          />
          <span>points</span>
          <TextField
            value={points}
            style={{ minWidth: "230px", display: "block", margin: "1px" }}
            id="outlined-basic"
            type="number"
            variant="outlined"
            onChange={(e) => setPoints(e.target.value)}
          />
        </DialogContent>

        <DialogActions>
          <Button autoFocus onClick={transferPoints} color="primary">
            Send
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
