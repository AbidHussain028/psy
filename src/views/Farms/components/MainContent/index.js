import React, { useState, useEffect } from "react";
import Web3 from "web3";
import { toast } from "react-toastify";
import { useWallet } from '@binance-chain/bsc-use-wallet'

import Dialog from "./dialog";
import DialogTransfer from "./dialogtransfer";
import "bootstrap/dist/css/bootstrap.min.css";
import "./content.css";
import bell from "../../../../assets/images/bell.jpg";
import logo from "../../../../assets/images/logo.jpg";
import logo2 from "../../../../assets/images/logo2.jpg";
import send from "../../../../assets/images/send.png";

import {
  contractAddress,
  tokeAddress,
  contractabi,
  tokenabi,
} from "../../../../utils/constant";

function Maincontent() {
  const { account ,ethereum} = useWallet()
console.log("aaa",account)
  let accountAd;
  const [account1, setAccount] = useState("Connect To Wallet");
  const [enteredAmount, setEnteredAmount] = useState(0);
  const [rewards, setRewards] = useState(0);
  const [balance, setBalance] = useState(0);
  const [reward, setReward] = useState(0);
  const [staked, setStaked] = useState(0);
  const [point, setPoint] = useState(0);
  const [message, setMessage] = useState({
    show: false,
    severity: "",
    message: "",
    title: "",
  });

  const [state, setState] = useState({
    mainAccount: null,
    totalZinTokens: null,
    rewards: null,
    stakeOf: null,
  });

  const loadWeb3 = async () => {
    let isConnected = false;
    try {
      if (window.ethereum) {
        const web3 = new Web3(ethereum)

        await window.ethereum.enable();

        isConnected = true;
      } else if (window.web3) {
        window.web3 = new Web3(window.web3.currentProvider);
        isConnected = true;
      } else {
        isConnected = false;
        // const obj = {
        //   show: true,
        //   severity: "error",
        //   message:
        //     "Metamask is not installed, please install it on your browser to connect.",
        // };
        // setMessage(obj);
      }
      if (isConnected === true) {
        const web3 = new Web3(ethereum)

        const contract = new web3.eth.Contract(contractabi, contractAddress);
        // const accounts = await getAccounts();
        // setAccount(accounts[0]);
        // accountAd = accounts[0];
        // getBalanceOfAccount();
        // getData();
        // setState({
        //   mainAccount: accounts[0],
        // });

        const accountDetails = null;
      
      }
    } catch (error) {
      console.log(error);
      const obj = {
        show: true,
        severity: "error",
        message: "Error while connecting metamask",
        title: "Connecting Metamask",
      };
      setMessage(obj);
    }
  };

  const getUpdateAccount = async (accounts) => {
    const web3 = new Web3(ethereum)

    const contract = new web3.eth.Contract(contractabi, contractAddress);
  };

  const getAccounts = async () => {
    const web3 = new Web3(ethereum)

    try {
      const accounts = await web3.eth.getAccounts();
      console.log(accounts);
      return accounts;
    } catch (error) {
      console.log("Error while fetching acounts: ", error);
      return null;
    }
  };

  // eslint-disable-next-line
  const isLockedAccount = async () => {
    try {
      const accounts = await getAccounts();
      if (accounts.length > 0) {
        console.log("Metamask is unlocked");
      } else {
        console.log("Metamask is locked");
      }
    } catch (error) {
      alert("Error while checking locked account");
    }
  };

  // eslint-disable-next-line
  const getBalanceOfAccount = async () => {
    try {
      const web3 = new Web3(ethereum)
      const contract = new web3.eth.Contract(tokenabi, tokeAddress);
      const accountDetails = await contract.methods.balanceOf(account).call();
      console.log("accountDetails", accountDetails);
      let blnc = accountDetails.toString();
      blnc = web3.utils.fromWei(blnc, "ether");
      console.log("accountDetails", blnc);
      setBalance(blnc);
    } catch (e) {
      console.log("error", e);
    }
  };

  const getData = async () => {
    try {
      const web3 = new Web3(ethereum)
      const contract = new web3.eth.Contract(contractabi, contractAddress);
      console.log("contract", contract);
      const accountDetails = await contract.methods
        .pointsCalculation(account)
        .call();
      console.log("reward", accountDetails);
      setReward(web3.utils.fromWei(accountDetails, "ether"));

      const accountDetais = await contract.methods
        .totalDeposited(account)
        .call();

      setStaked(web3.utils.fromWei(accountDetais), "ether");

      const accountDetai = await contract.methods.users(account).call();
      console.log("points", accountDetai);
      setPoint(web3.utils.fromWei(accountDetai), "ether");
    } catch (e) {
      console.log("error", e);
    }
  };

  const unstake = async () => {
    const web3 = new Web3(ethereum)
    const contract = new web3.eth.Contract(contractabi, contractAddress);
    const accountDetails = await contract.methods
      .unstake()
      .send({
        from: account,
        gasLimit: 210000,
      })
      .on("transactionHash", async (hash) => {
        toast.info("Your transaction is pending");
      })
      .on("receipt", async (receipt) => {
        toast.success("Your transaction is confirmed");
        console.log("confrimed", receipt);
      })
      .on("error", async (error) => {
        console.log("error", error);
        toast.error("User denied transaction");
      });
    console.log("accountDetails", accountDetails);
  };
  const withdraw = async () => {
    // const web3 = window.web3;
    // unstake
    const web3 = new Web3(ethereum)
    const contract = new web3.eth.Contract(contractabi, contractAddress);
    const accountDetails = await contract.methods
      .withdraw()
      .send({
        from: account,
        gasLimit: 210000,
      })
      .on("transactionHash", async (hash) => {
        toast.info("Your transaction is pending");
      })
      .on("receipt", async (receipt) => {
        toast.success("Your transaction is confirmed");
        console.log("confrimed", receipt);
      })
      .on("error", async (error) => {
        console.log("error", error);
        toast.error("User denied transaction");
      });
    console.log("accountDetails", accountDetails);
  };

  const [openModel, setOpenModel] = useState(false);
  const [openModelt, setOpenModelt] = useState(false);
  const handleOpenModel = () => {
    setOpenModel(true);
  };
  const handleCloseModel = () => {
    setOpenModel(false);
  };

  const handleOpenModelt = () => {
    setOpenModelt(true);
  };
  const handleCloseModelt = () => {
    setOpenModelt(false);
  };

  const [mainAccountDetails, setMainAccountDetails] = useState(null);

  useEffect(() => {
    setInterval(() => {
      if (account) {
        loadWeb3();
      } else {
        loadWeb3();
      }
    }, 2000);

    // eslint-disable-next-line
  }, []);

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-3">
          <Dialog
            dialogueAccount={account}
            handleCloseModel={handleCloseModel}
            open={openModel}
          />

          <DialogTransfer
            dialogueAccount={account}
            handleCloseModel={handleCloseModelt}
            open={openModelt}
          />
        </div>
        <div className="col-md-6">
          <div className="paragon" id="#home">
            <div className="psypower">
              <img alt="img" className="bodylogo" src={bell} />
              <span>PSY-PSY POWER</span>
            </div>
            <div>
              <div>
                <img alt="img" className="logo" src={logo2} />
                <img alt="img" className="logo" src={logo} />
              </div>
            </div>
            <div className="calculation">
              <div className="row">
                <span className="col-sm-2"> </span>
                <span className="col-sm-4">wallet:</span>
                <span className="col-sm-5"> {balance} PSY</span>
              </div>
              {/* </div>
            <div className="calculation"> */}
              <div className="row">
                <span className="col-sm-2"> </span>
                <span className="col-sm-4"> Points: </span>
                <span className="col-sm-5">{point}</span>
              </div>
              <div className="row">
                <span className="col-sm-2"> </span>
                <span className="col-sm-4"> Staked: </span>
                <span className="col-sm-5">{staked}</span>
              </div>
              <div className="row">
                <span className="col-sm-2"> </span>
                <span className="col-sm-4">Rewards:</span>
                <span className="col-sm-4">{reward}</span>

                <img
                  alt="img"
                  className="col-sm-2 send"
                  src={send}
                  onClick={handleOpenModelt}
                  role="presentation"
                />
              </div>
            </div>
            <div className="spacdiv">
              <div className="button-group">
                <button type="button" className="btn" onClick={handleOpenModel}>
                  Approve & stake
                </button>
              </div>
              <div className="button-group">
                <button type="button" className="btn" onClick={unstake}>
                  Unstake
                </button>
              </div>
              <div className="button-group">
                <button type="button" className="btn" onClick={withdraw}>
                  Redeem
                </button>
              </div>
              <div className="button-group">
                <button type="button" className="btn">
                  Buy NFT
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col d-flex justify-content-center mb-3">
          <p
            style={{
            
              color: "red",
              fontSize: "12pt",
            }}
          >
            *Buy NFT Function Will work after NFT market open
          </p>
        </div>
      </div>
    </div>
  );
}

export default Maincontent;
