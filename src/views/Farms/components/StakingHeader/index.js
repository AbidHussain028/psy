import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import mainlogo from "../../../../assets/images/mainlogo.jpg";
import "./header.css";
import { contractAddress } from "../../../../utils/constant";

function Header() {
  const [account, setAccount] = useState("Connect To Wallet");
  const [mainAccountDetails, setMainAccountDetails] = useState(null);

  const loadWeb3 = async () => {
    // console.log("contract Header");
    let isConnected = false;
    let connection;
    let mainAccount;
    try {
      if (
        window.tronWeb &&
        window.tronWeb.defaultAddress.base58 === "undefined"
      ) {
        connection = "TROn LINK is not available";
        // console.log("not available");
        isConnected = false;
        // console.log("Tron is not installed, please install it on your browser to connect.");
      } else {
        connection = "Connected to Tron LINK.";
        console.log("connections : ", connection);
        isConnected = true;
        mainAccount = await window.tronWeb.defaultAddress.base58;
        // console.log("MAINNACCOUNt : ", mainAccount);
        isLocked();
        // getUserInfor();
        // getTotalReward();
        loadBlockchainData();
        if (mainAccount) {
          if (isConnected === true) {
            // console.log("connections : ", connection);
            mainAccount = await window?.tronWeb?.defaultAddress?.base58;
            setAccount(mainAccount);
            const accountDetails = null;
            localStorage.setItem("load", mainAccount);
            setMainAccountDetails(accountDetails);
            loadBlockchainData();
          } else {
            // console.log("Tron Not Connected");
          }
        } else {
          // console.log("Please login or install tron wallet!");
          // alert("Please login or install tron wallet!");
        }
      }
    } catch (error) {
      // console.log("error0", error);
    }
  };

  function isLocked() {
    if (window.tronWeb.defaultAddress.base58 == null) {
      // console.log("error null");
    } else if (window.tronWeb.defaultAddress.base58 === 0) {
      // console.log("TRON LINK is locked");
    } else {
      // console.log("TRON LINK is unlocked");
    }
  }

  const loadBlockchainData = async () => {
    try {
      // console.log("contract called");
      const contract = await window?.tronWeb.contract().at(contractAddress);
      // sellPriceCalculation
      const noOfFiles = await contract.sell().call();
    } catch (e) {
      console.log(e);
    }
  };

  // useEffect(() => {
  //   loadWeb3();
  //   setInterval(() => {
  //     if (account) {
  //       loadWeb3();
  //     } else {
  //       loadWeb3();
  //     }
  //   }, 2000);
  //   // eslint-disable-next-line
  // }, []);

  return (
    <div className="container">
      <h1>Psychic NFT Card Want?</h1>
      <div className="row">
        <div className="col-md">
          <p className="textmain">
            Stake PSY token to earn rareNFT Card. <br />
            You can Unstake anytime.
            <br />
            PSY rewards are calculated per block
            <br />
            <br />
            100 PSY staking = 0.2 point a day
            <br />1 Point = 1 random card
          </p>
        </div>
        <div className="col-md">
          <img className="mainlogo" src={mainlogo} alt="logo" />
        </div>
      </div>
    </div>
  );
}

export default Header;
