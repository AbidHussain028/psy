import React, { useEffect, useCallback } from "react";
import styled from "styled-components";
import { Route, useLocation, useRouteMatch } from "react-router-dom";
import { useDispatch } from "react-redux";
import BigNumber from "bignumber.js";
import { useWallet } from "@binance-chain/bsc-use-wallet";
import { provider } from "web3-core";
import { Image, Heading } from "@pancakeswap-libs/uikit";
import { BLOCKS_PER_YEAR } from "config";
import FlexLayout from "components/layout/Flex";
import Page from "components/layout/Page";
import { useFarms, usePriceBnbBusd, usePriceCakeBusd } from "state/hooks";
import useRefresh from "hooks/useRefresh";
import { fetchFarmUserDataAsync } from "state/actions";
import { QuoteToken } from "config/constants/types";
import useI18n from "hooks/useI18n";
import useTheme from "hooks/useTheme";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import FarmCard, { FarmWithStakedValue } from "./components/FarmCard/FarmCard";
import FarmTabButtons from "./components/FarmTabButtons";
import Divider from "./components/Divider";
import Responsivenav from "./components/ResponsiveNav";
import StakingHeader from "./components/StakingHeader";
import MainContent from "./components/MainContent";
import Footer from "./components/Footer";

export interface FarmsProps {
  tokenMode?: boolean;
}

const StyledDiv = styled.div`
  .mobile {
    ${({ theme }) => theme.mediaQueries.nav} {
      display: none;
    }
  }
  .desktop {
    display: none;
    ${({ theme }) => theme.mediaQueries.nav} {
      display: block;
    }
  }
`;

const Farms: React.FC<FarmsProps> = (farmsProps) => {
  const location = useLocation();
  const { path } = useRouteMatch();
  const TranslateString = useI18n();
  const farmsLP = useFarms();
  const cakePrice = usePriceCakeBusd();
  const bnbPrice = usePriceBnbBusd();
  const {
    account,
    ethereum,
  }: { account: string; ethereum: provider } = useWallet();
  const { tokenMode } = farmsProps;
  const { isDark } = useTheme();

  const dispatch = useDispatch();
  const { fastRefresh } = useRefresh();
  useEffect(() => {
    if (account) {
      dispatch(fetchFarmUserDataAsync(account));
    }
  }, [account, dispatch, fastRefresh]);

  const activeFarms = farmsLP.filter(
    (farm) => !!farm.isTokenOnly === !!tokenMode && farm.multiplier !== "0X"
  );
  const inactiveFarms = farmsLP.filter(
    (farm) => !!farm.isTokenOnly === !!tokenMode && farm.multiplier === "0X"
  );

  // /!\ This function will be removed soon
  // This function compute the APY for each farm and will be replaced when we have a reliable API
  // to retrieve assets prices against USD
  const farmsList = useCallback(
    (farmsToDisplay, removed: boolean) => {
      // const cakePriceVsBNB = new BigNumber(farmsLP.find((farm) => farm.pid === CAKE_POOL_PID)?.tokenPriceVsQuote || 0)
      const farmsToDisplayWithAPY: FarmWithStakedValue[] = farmsToDisplay.map(
        (farm) => {
          // if (!farm.tokenAmount || !farm.lpTotalInQuoteToken || !farm.lpTotalInQuoteToken) {
          //   return farm
          // }
          const cakeRewardPerBlock = new BigNumber(farm.REWARD_PER_BLOCK || 1)
            .times(new BigNumber(farm.poolWeight))
            .div(new BigNumber(10).pow(18));
          const cakeRewardPerYear = cakeRewardPerBlock.times(BLOCKS_PER_YEAR);

          let apy = cakePrice.times(cakeRewardPerYear);

          let totalValue = new BigNumber(farm.lpTotalInQuoteToken || 0);

          if (farm.quoteTokenSymbol === QuoteToken.BNB) {
            totalValue = totalValue.times(bnbPrice);
          }

          if (totalValue.comparedTo(0) > 0) {
            apy = apy.div(totalValue);
          }

          return { ...farm, apy };
        }
      );
      return farmsToDisplayWithAPY.map((farm) => (
        <FarmCard
          key={farm.pid}
          farm={farm}
          removed={removed}
          bnbPrice={bnbPrice}
          cakePrice={cakePrice}
          ethereum={ethereum}
          account={account}
        />
      ));
    },
    [bnbPrice, account, cakePrice, ethereum]
  );

  return (
    <Page>
      {tokenMode ? (
        <StyledDiv>
          {/* <div className="desktop">
              {isDark ? <><Image src="/images/staking_dark.png" alt="soom" width={888} height={876} responsive />
              <img src="/images/coming_soon_night.png" alt="coming" width={888} height={660}  /></>
              : <><Image src="/images/staking_day.png" alt="soom" width={888} height={876} responsive />
              <img src="/images/coming_soon_day.png" alt="coming" width={888} height={660} /></>}
            </div>
            <div className="mobile">
              {isDark? <><Image src="/images/staking_m_night.png" alt="staking" width={360} height={920} responsive  />
              <img src="/images/coming_soon_night.png" alt="coming" width={380} height={275}  /></>
              : <><Image src="/images/staking_m_day.png" alt="staking" width={360} height={920} responsive  />
                <img src="/images/coming_soon_night.png" alt="coming" width={380} height={275}  /></>
              }
            </div> */}

          <div className="desktop">
            {/* <Responsivenav /> */}
            <StakingHeader />
            <MainContent />
            <Footer />
            {/* <ToastContainer /> */}
          </div>
        </StyledDiv>
      ) : (
        <>
          <Heading
            as="h1"
            size="lg"
            color="tertiary"
            mb="50px"
            style={{ textAlign: "center" }}
          >
            {tokenMode
              ? TranslateString(10002, "Stake tokens to earn PSY")
              : TranslateString(320, "Stake LP tokens to earn PSY")}
          </Heading>
          <FarmTabButtons />
          <div>
            <Divider />
            <FlexLayout>
              <Route exact path={`${path}`}>
                {farmsList(activeFarms, false)}
              </Route>
              <Route exact path={`${path}/history`}>
                {farmsList(inactiveFarms, true)}
              </Route>
            </FlexLayout>
          </div>
          {
            // eslint-disable-next-line no-nested-ternary
            location.pathname === `${path}` ? (
              isDark ? (
                <Image
                  src="/images/egg/wait_frog_night.png"
                  alt="illustration"
                  width={1300}
                  height={700}
                  responsive
                />
              ) : (
                <Image
                  src="/images/egg/wait_frog_day.png"
                  alt="illustration"
                  width={1300}
                  height={700}
                  responsive
                />
              )
            ) : isDark ? (
              <Image
                src="/images/egg/n_wait_frog_night.png"
                alt="illustration"
                width={1300}
                height={700}
                responsive
              />
            ) : (
              <Image
                src="/images/egg/n_wait_frog_day.png"
                alt="illustration"
                width={1300}
                height={700}
                responsive
              />
            )
          }
        </>
      )}
    </Page>
  );
};

export default Farms;
