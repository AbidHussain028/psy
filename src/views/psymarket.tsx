import React, {useState, useEffect} from 'react'
import styled from 'styled-components'
import Page from 'components/layout/Page'
import useTheme from 'hooks/useTheme';
import ReactPlayer from 'react-player';
import { Image } from '@pancakeswap-libs/uikit'

const StyledNotFound = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  // height: calc(100vh - 54px);
  justify-content: center;
`

// const CardImage = styled.img`
//   margin-bottom: 16px;
// `

// Hook
export function useWindowSize() {
  // Initialize state with undefined width/height so server and client renders match
  // Learn more here: https://joshwcomeau.com/react/the-perils-of-rehydration/
  const [windowSize, setWindowSize] = useState({
    width: undefined,
    height: undefined,
  });

  useEffect(() => {
    // Handler to call on window resize
    function handleResize() {
      // Set window width/height to state
      setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    }
    
    // Add event listener
    window.addEventListener("resize", handleResize);
    
    // Call handler right away so state gets updated with initial window size
    handleResize();
    
    // Remove event listener on cleanup
    return () => window.removeEventListener("resize", handleResize);
  }, []); // Empty array ensures that effect is only run on mount

  return windowSize;
}

const NotFound = () => {
  // const TranslateString = useI18n()
  const { isDark } = useTheme();
  const size = useWindowSize()

  return (
    <Page>
      <StyledNotFound>
        <ReactPlayer 
          url="https://youtu.be/ye-QibISFKI"
          width={size.width >= 992 ? "80%" : "100%"}
          controls
        />
        
        <img src="/images/final pepe.gif" alt=""/>
        {isDark ? <Image src="/images/market_night.jpg" alt="market" width={1005} height={1280} responsive />
        :<Image src="/images/market_day.jpg" alt="market" width={1005} height={1100} responsive />}
        {isDark ? <Image src="/images/coming_soon_night.png" alt="coming" width={1005} height={580} responsive  />
        :<Image src="/images/coming_soon_day.png" alt="coming" width={1005} height={580} responsive />}
      </StyledNotFound>
    </Page>
  )
}

export default NotFound
