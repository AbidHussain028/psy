import { MenuEntry } from '@pancakeswap-libs/uikit'

const config: MenuEntry[] = [
  {
    label: 'Home',
    icon: 'HomeIcon',
    href: '/',
  },
  {
    label: 'Exchange',
    icon: 'TradeIcon',
    href: 'https://exchange.pancakeswap.finance/#/swap?outputCurrency=0x8dbc995946ad745dd77186d1ac10019b8ea6694a',
  },
  {
    label: 'Farming',
    icon: 'FarmIcon',
    href: '/farms',
  },
  {
    label: 'Staking',
    icon: 'IfoIcon',
    href: '/staking',
  },
  {
    label: 'Psychic NFT',
    icon: 'PoolIcon',
    href: '/market',
  },
  {
    label: 'Predict',
    icon: 'TicketIcon',
    href: '/predict',
  },
  {
    label: 'What is Psy?',
    icon: 'NftIcon',
    href: 'https://psychicfinance.medium.com/psychic-finance-a-crypto-farming-and-predictive-market-with-a-difference-8a108fdb009d',
  },
  {
    label: 'Whitepaper',
    icon: 'WpIcon',
    href: 'https://drive.google.com/file/d/14DDkhMcJRHiPB8DVCQVaK3PEvuGtjTim/view?ths=true',
  }
]

export default config
